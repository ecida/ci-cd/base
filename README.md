# CICD

## Overview and Purpose
This GitLab repository serves as a central location for storing base GitLab CI/CD YAML files that can be reused across multiple repositories. It aims to provide a standardized and efficient approach to Continuous Integration and Continuous Deployment (CI/CD) processes within your organization.

The purpose of this repository is to streamline the configuration and setup of CI/CD pipelines for various projects. By centralizing the common CI/CD configurations, it becomes easier to maintain consistency, reduce duplication of effort, and promote best practices across different repositories.

## Usage
To utilize the CI/CD YAML files in this repository, follow the steps below:

1. Copy the following YAML code to your project's ``.gitlab-ci.yml`` file:

    ```yaml
    include:
        # Adds the 'base ci/cd' process
        - project: 'ecida/life-cycle-management/cicd'
            ref: main
            file: '<ci_yaml_file>'
    ```
2. Replace ``<ci_yaml_file>`` with the corresponding file.

3. Commit the changes to your project's repository.

### Source Path Annotation

The `source-path-annotation.yaml` script is used to automatically add/update the `ui.ecida.org/source-path` annotation for pipeline YAML files. It does the following:
1. Checks changed pipeline files since the commit tagged with the `annotated` tag;
2. Manipulates all changed pipelines to include the correct `ui.ecida.org/source-path` annotation;
3. Commits changes and tags the commit with a Git tag named `annotated`;
4. Pushes the tag (and commit if needed).

Usage notes:
- This script would be enabled on the repository containing pipeline YAML files.
- It requires an access token with write scope in the `ACCESS_TOKEN` variable.
    - This can be set up through the repository settings.
- To prevent merge conflicts etc. it is important to enable these CICD settings on the repository:
    - [x] Auto-cancel redundant pipelines
    - [x] Prevent outdated deployment jobs
